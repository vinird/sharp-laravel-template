@extends('adminlte::layouts.app')

<!-- Estilos -->
@section('custom-header')
	@include('profile.partials.sup.styles')
@endsection


@section('main-content')

	<!-- Información del perfil -->
	@include('profile.partials.profile-main-view')

	<!-- Modal para modificar el perfil -->
	@include('profile.partials.sup.modal-update-profile')

	<!-- Modal para modificar foto de portada -->
	@include('profile.partials.sup.modal-change-main-photo')

	<!-- Modal para modificar foto de portada -->
	@include('profile.partials.sup.modal-delete-posts')

	<!-- Modal para modificar foto de portada -->
	@include('profile.partials.sup.modal-change-profile-image')

@endsection

<!-- Script -->
@section('custom-scripts')
	@include('profile.partials.sup.scripts')
@endsection
<!-- /.box-header -->
<div class="container">
<br>
	<div class="box-body no-padding">
		<ul class="users-list clearfix">
			<li>
				<img src="{{ Gravatar::get($user->email) }}" alt="User Image">
				<a class="users-list-name" href="#">Alexander Pierce</a>
				<span class="users-list-date">Today</span>
			</li>
			<li>
				<img src="{{ Gravatar::get($user->email) }}" alt="User Image">
				<a class="users-list-name" href="#">Norman</a>
				<span class="users-list-date">Yesterday</span>
			</li>
			<li>
				<img src="{{ Gravatar::get($user->email) }}" alt="User Image">
				<a class="users-list-name" href="#">Jane</a>
				<span class="users-list-date">12 Jan</span>
			</li>
			<li>
				<img src="{{ Gravatar::get($user->email) }}" alt="User Image">
				<a class="users-list-name" href="#">John</a>
				<span class="users-list-date">12 Jan</span>
			</li>
			<li>
				<img src="{{ Gravatar::get($user->email) }}" alt="User Image">
				<a class="users-list-name" href="#">Alexander</a>
				<span class="users-list-date">13 Jan</span>
			</li>
			<li>
				<img src="{{ Gravatar::get($user->email) }}" alt="User Image">
				<a class="users-list-name" href="#">Sarah</a>
				<span class="users-list-date">14 Jan</span>
			</li>
			<li>
				<img src="{{ Gravatar::get($user->email) }}" alt="User Image">
				<a class="users-list-name" href="#">Nora</a>
				<span class="users-list-date">15 Jan</span>
			</li>
			<li>
				<img src="{{ Gravatar::get($user->email) }}" alt="User Image">
				<a class="users-list-name" href="#">Nadia</a>
				<span class="users-list-date">15 Jan</span>
			</li>
			<li>
				<img src="{{ Gravatar::get($user->email) }}" alt="User Image">
				<a class="users-list-name" href="#">Alexander Pierce</a>
				<span class="users-list-date">Today</span>
			</li>
			<li>
				<img src="{{ Gravatar::get($user->email) }}" alt="User Image">
				<a class="users-list-name" href="#">Norman</a>
				<span class="users-list-date">Yesterday</span>
			</li>
			<li>
				<img src="{{ Gravatar::get($user->email) }}" alt="User Image">
				<a class="users-list-name" href="#">Jane</a>
				<span class="users-list-date">12 Jan</span>
			</li>
			<li>
				<img src="{{ Gravatar::get($user->email) }}" alt="User Image">
				<a class="users-list-name" href="#">John</a>
				<span class="users-list-date">12 Jan</span>
			</li>
			<li>
				<img src="{{ Gravatar::get($user->email) }}" alt="User Image">
				<a class="users-list-name" href="#">Alexander</a>
				<span class="users-list-date">13 Jan</span>
			</li>
			<li>
				<img src="{{ Gravatar::get($user->email) }}" alt="User Image">
				<a class="users-list-name" href="#">Sarah</a>
				<span class="users-list-date">14 Jan</span>
			</li>
			<li>
				<img src="{{ Gravatar::get($user->email) }}" alt="User Image">
				<a class="users-list-name" href="#">Nora</a>
				<span class="users-list-date">15 Jan</span>
			</li>
			<li>
				<img src="{{ Gravatar::get($user->email) }}" alt="User Image">
				<a class="users-list-name" href="#">Nadia</a>
				<span class="users-list-date">15 Jan</span>
			</li>
		</ul>
		<!-- /.users-list -->
	</div>
	<div class="box-footer text-center">
		<a href="javascript:void(0)" class="uppercase">Ver todos los amigos</a>
	</div>	
</div>

<div class="container">
	<div class="row">
		<div class="flex-images images">
			<div class="item" data-w="400" data-h="400"><img src="http://lorempixel.com/g/500/450"></div>
			<div class="item" data-w="400" data-h="400"><img src="http://lorempixel.com/g/770/650"></div>
			<div class="item" data-w="400" data-h="400"><img src="http://lorempixel.com/g/800/750"></div>
			<div class="item" data-w="400" data-h="400"><img src="http://lorempixel.com/g/500/300"></div>
			<div class="item" data-w="400" data-h="400"><img src="http://lorempixel.com/g/500/350"></div>
			<div class="item" data-w="400" data-h="400"><img src="http://lorempixel.com/g/400/350"></div>
			<div class="item" data-w="400" data-h="400"><img src="http://lorempixel.com/g/550/350"></div>
			<div class="item" data-w="400" data-h="400"><img src="http://lorempixel.com/g/600/350"></div>
			<div class="item" data-w="400" data-h="400"><img src="http://lorempixel.com/g/700/350"></div>
			<div class="item" data-w="400" data-h="400"><img src="http://lorempixel.com/g/650/350"></div>
			<div class="item" data-w="400" data-h="400"><img src="http://lorempixel.com/g/430/350"></div>
			<div class="item" data-w="400" data-h="400"><img src="http://lorempixel.com/g/530/350"></div>
			<div class="item" data-w="400" data-h="400"><img src="http://lorempixel.com/g/520/350"></div>
			<div class="item" data-w="400" data-h="400"><img src="http://lorempixel.com/g/510/350"></div>
		</div>
		<div class="text-center">
			<nav aria-label="Page navigation">
				<ul class="pagination">
					<li>
						<a href="#" aria-label="Previous">
							<span aria-hidden="true">&laquo;</span>
						</a>
					</li>
					<li><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
					<li>
						<a href="#" aria-label="Next">
							<span aria-hidden="true">&raquo;</span>
						</a>
					</li>
				</ul>
			</nav>
			<!-- /.box-tools -->
		</div>
	</div>

</div>

<div class="container-fluid margin-none container-main-profile-image">
	<div class="box box-widget widget-user">
		<!-- Add the bg color to the header using any of the bg-* classes -->
		<div class="widget-user-header bg-black" id="main-profile-image" style="background: url('http://lorempixel.com/g/1200/300') center center;background-size: cover;   min-height: 500px;">
		</div>
		<div class="widget-user-image" style="top: 200px;">
			<img class="img-circle" src="{{ Gravatar::get($user->email) }}" alt="User Avatar">
		</div>
		<div class="profile-info-image">
			<h3 class="widget-user-username text-center">Elizabeth Pierce</h3>
			<h5 class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam, tempore!</h5>
			<div id="btn-main-profile-image">
				<a class="btn bg-purple btn-sm" onclick="abrirModalModificarFotoPortada()"><i class="fa fa-pencil" aria-hidden="true"></i></a>
			</div>
		</div>
		<div class="box-footer">
			<div class="row">
				<!-- /.col -->
				<div class="col-sm-6 border-right">
					<div class="description-block">
						<h5 class="description-header">13,000</h5>
						<span class="description-text">AMIGOS</span>
					</div>
					<!-- /.description-block -->
				</div>
				<!-- /.col -->
				<div class="col-sm-6">
					<div class="description-block">
						<h5 class="description-header">35</h5>
						<span class="description-text">PUBLICACIONES</span>
					</div>
					<!-- /.description-block -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<div class="box-footer">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
						<!-- Nav tabs -->
						<ul class="nav nav-pills btn-group nav-justified" role="tablist">
							<li role="presentation" class="active">
								<a href="#home" aria-controls="home" role="tab" data-toggle="tab">Fotos</a>
							</li>
							<li role="presentation">
								<a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Perfil</a>
							</li>
							<li role="presentation">
								<a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Amigos</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="home">
					@include('profile.partials.gallery')
				</div>
				<div role="tabpanel" class="tab-pane fade" id="profile">
					@include('profile.partials.profile-info')
				</div>
				<div role="tabpanel" class="tab-pane fade" id="messages">
					@include('profile.partials.friends')
				</div>
			</div>
			<!-- Galería -->

		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="box box-primary">
				<!-- /.box-header -->
				<div class="box-body box-profile">
					<br>
					<img class="profile-user-img img-responsive img-circle" src="{{ Gravatar::get($user->email) }}" alt="User profile picture">
					<h4 class="profile-username text-center"><a href="#" onclick="abrirModalModificarImagenPerfil()"><i class="fa fa-camera" aria-hidden="true"></i></a></h4>
					<h3 class="profile-username text-center">Nina Mcintire</h3>

					<p class="text-muted text-center">Software Engineer</p>

					<p class="text-right">
						<a onclick="abrirModalModificarPerfil()" class="btn btn-default btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i> </a> 
						Actualizar información 
					</p>
					<ul class="list-group list-group-unbordered">
						<li class="list-group-item">
							<b>Amigos</b> <a class="pull-right">1,322</a>
						</li>
						<li class="list-group-item">
							<b>Publicaciones </b> 
							<a onclick="abrirModalEliminarPublicacion()" class="btn btn-default btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i> </a>
							<a class="pull-right">543</a>
						</li>
					</ul>
					<strong><i class="fa fa-book margin-r-5"></i> Educación</strong>
					<p class="text-muted">
						B.S. in Computer Science from the University of Tennessee at Knoxville
					</p>
					<hr>
					<strong><i class="fa fa-briefcase margin-r-5"></i> Profesión</strong>
					<p class="text-muted">
						Software Engineer
					</p>
					<hr>
					<strong><i class="fa fa-map-marker margin-r-5"></i> Vivo en</strong>
					<p class="text-muted">Malibu, California</p>
					<hr>
					<strong><i class="fa fa-quote-left margin-r-5"></i> Nota</strong>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
					<hr>
					<strong><i class="fa fa-file-text-o margin-r-5"></i> Biografía</strong>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum..</p>
					<hr>
					<a href="#" class="btn btn-primary btn-block">
						<i class="fa fa-user-plus" aria-hidden="true"></i>
						<b> Agregar amigo</b>
					</a>
				</div>
				<!-- /.box-body -->
			</div>
		</div>
	</div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modalModificarPerfil">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Información del perfil</h4>
			</div>
			<div class="modal-body">
				
				<form class="form-horizontal" autocomplete="off">
					<div class="box-body">
						<div class="form-group form-group-sm">
							<label  class="col-sm-2 control-label">Email</label>
							<div class="col-sm-10">
								<input type="email" class="form-control black-color">
							</div>
						</div>
						<div class="form-group form-group-sm">
							<label class="col-sm-2 control-label">Educación</label>
							<div class="col-sm-10">
								<input type="text" name="educacion" class="form-control black-color">
							</div>
						</div>
						<div class="form-group form-group-sm">
							<label class="col-sm-2 control-label">Profeción</label>
							<div class="col-sm-10">
								<input type="text" name="profecion" class="form-control black-color">
							</div>
						</div>
						<div class="form-group form-group-sm">
							<label class="col-sm-2 control-label">Residencia</label>
							<div class="col-sm-10">
								<input type="text" name="residencia" class="form-control black-color">
							</div>
						</div>
						<div class="form-group form-group-sm">
							<label class="col-sm-2 control-label">Nota</label>
							<div class="col-sm-10">
								<input type="text" name="nota" class="form-control black-color">
							</div>
						</div>
						<div class="form-group form-group-sm">
							<label class="col-sm-2 control-label">Biografía</label>
							<div class="col-sm-10">
								<textarea name="biografia" class="form-control black-color"></textarea>
							</div>
						</div>
					</div>
					<!-- /.box-body -->
				</form>
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</button>
				<button type="button" class="btn btn-primary btn-sm"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar cambios</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
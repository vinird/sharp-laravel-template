<div class="modal fade" tabindex="-1" role="dialog" id="modal-delete-posts">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Eliminar publicaciones</h4>
			</div>
			<div class="box box-primary direct-chat direct-chat-primary">
				<!-- /.box-header -->
				<div class="box-body" style="max-height: 500px; overflow-y: scroll;">
					<table class="table">
						<thead>
							<th></th>
							<th></th>
							<th></th>
						</thead>
						<tbody>
							<tr>
								<td>
									<img class="img-responsive" style="min-width: 80px;" src="http://lorempixel.com/g/400/100">
								</td>
								<td>
									<span class="text-muted">127 likes - 3 comments</span>
									<br>
									<p>
										lorem wer qwjn wqe iqwjenqwiej wiqjen iwqejn orem wer qwjn wqe iqwjenqwiej wiqjen iwqejn orem wer qwjn wqe iqwjenqwiej wiqjen iwqejn orem wer qwjn wqe iqwjenqwiej wiqjen iwqejn
									</p>
								</td>
								<td>
									<button href="" class="btn btn-default btn-sm btn-eliminar-post" post-id="" onclick="btnConfirmarEliminarPost(this)">
										<span class="hidden-xs">Eliminar</span>
										<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-trash" aria-hidden="true"></i></span>
									</button>
								</td>
							</tr>
							<tr>
								<td>
									<img class="img-responsive" style="min-width: 80px;" src="http://lorempixel.com/g/100/100">
								</td>
								<td>
									<span class="text-muted">127 likes - 3 comments</span>
									<br>
									<p>
										lorem wer qwjn wqe iqwjenqwiej wiqjen iwqejn
									</p>
								</td>
								<td>
									<button href="" class="btn btn-default btn-sm btn-eliminar-post" post-id="" onclick="btnConfirmarEliminarPost(this)">
										<span class="hidden-xs">Eliminar</span>
										<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-trash" aria-hidden="true"></i></span>
									</button>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="overlay">
					<i class="fa fa-refresh fa-spin"></i>
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
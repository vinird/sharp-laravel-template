<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="es">

@section('htmlheader')
@include('adminlte::layouts.partials.htmlheader')
@show

@yield('custom-header')
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-blue fixed layout-top-nav skin-purple-light">
    <div id="app">
        <div class="wrapper"> 

            <div>
                @include('adminlte::layouts.partials.mainheader')
            </div>


            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper" style="min-height: 886px; border: none">
                <!-- Main content -->
                <section class="">
                    <!-- Your Page Content Here -->
                    @yield('main-content')
                </section><!-- /.content -->
            </div><!-- /.content-wrapper -->


            @include('adminlte::layouts.partials.footer')

            <!-- Modal mensajes -->
            @include('vendor.adminlte.layouts.partials-home.modal-messages')

            <!-- Modal notificaciones -->
            @include('vendor.adminlte.layouts.partials-home.modal-notifications')

             <!-- Modal cambiar contraseña -->
            @include('vendor.adminlte.layouts.partials-home.modal-change-password')

            <!-- Modal cambiar contraseña -->
            @include('vendor.adminlte.layouts.partials-home.modal-share-post')

        </div><!-- ./wrapper -->
    </div>
    @section('scripts')
    @include('adminlte::layouts.partials.scripts')
    @show

    <!-- Custom scripts -->
    @yield('custom-scripts')

</body>
</html>

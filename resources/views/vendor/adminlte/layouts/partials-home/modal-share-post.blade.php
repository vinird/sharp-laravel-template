<div class="modal fade" tabindex="-1" role="dialog" id="modal-share-post">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Compartir publicación</h4>
			</div>
			<div class="modal-body">
				
				<form class="form-horizontal" autocomplete="off">
					<div class="form-group form-group-sm">
						<label class="col-sm-2 control-label">Link: </label>
						<div class="col-sm-10">
							<input type="text" class="form-control black-color" id="shareInputValue">
							<span class="help-block">Puede utilizar este link para compartir la publicación.</span>
						</div>
					</div>
					<!-- /.box-body -->
				</form>
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
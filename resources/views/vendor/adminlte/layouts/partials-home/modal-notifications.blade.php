<div class="modal fade" id="modal-all-notifications" tabindex="1" role="dialog" aria-labelledby="modal-all-notifications">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Notificaciones</h4>
            </div>
            <div class="box box-warning">
                <!-- /.box-header -->
                <div class="box-body" style="max-height: 500px; overflow-y: scroll;">
                    <div class="well box-comments">
                        <div class="box-comment">
                            <img src="http://www.gravatar.com/avatar/64e1b8d34f425d19e1ee2ea7236d3028.jpg?s=80&amp;d=mm&amp;r=g" alt="User Image" class="img-circle img-sm"> 
                            <div class="comment-text">
                                <span class="username"> Maria Gonzales <span class="text-muted pull-right">8:03 PM Today</span></span>
                                It is a long established fact that a reader will be distracted
                                by the readable content of a page when looking at its layout.
                            </div> 
                        </div>
                    </div>
                </div>
                <div class="overlay">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                </div>
                <!-- /.box-footer-->
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- Nuevo mensaje -->

<div class="modal fade" id="modal-see-notification" tabindex="1" role="dialog" aria-labelledby="modal-see-notification">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Notificación</h4>
            </div>
            <div class="modal-body">
                <div class="well box-comments">
                    <div class="box-comment">
                        <img src="http://www.gravatar.com/avatar/64e1b8d34f425d19e1ee2ea7236d3028.jpg?s=80&amp;d=mm&amp;r=g" alt="User Image" class="img-circle img-sm"> 
                        <div class="comment-text">
                            <span class="username"> Maria Gonzales <span class="text-muted pull-right">8:03 PM Today</span></span>
                            It is a long established fact that a reader will be distracted
                            by the readable content of a page when looking at its layout.
                        </div> 
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>




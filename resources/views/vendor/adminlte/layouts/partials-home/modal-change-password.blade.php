<!-- Cambiar contraseña  -->
<div class="modal fade" id="modal-change-password" tabindex="1" role="dialog" aria-labelledby="modal-change-password">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Cambio de contraseña</h4>
            </div>
            <div class="modal-body">
                <div class="form-group form-group-sm">
                <label for="contrasenaActual">Contraseña actual</label>
                  <input type="text" class="form-control" id="contrasenaActual" placeholder="Digite su contraseña.." required>
              </div>
              <hr>
              <div class="form-group form-group-sm">
                  <label for="nuevaContrasena">Nueva contraseña</label>
                  <input type="text" class="form-control" id="nuevaContrasena" placeholder="Digite la nueva contraseña.." required="">
              </div>
              <div class="form-group form-group-sm">
                  <label for="confirmarContrasena">Confirme la contraseña</label>
                  <input type="text" class="form-control" id="confirmarContrasena" placeholder="Ingrese la nueva contraseña..">
              </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</button>
            <button type="button" class="btn btn-primary btn-sm" >Enviar</button>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>




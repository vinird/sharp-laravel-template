<div class="col-sm-12 col-md-3">
	<div class="box box-primary">
		<div class="box-body box-profile">
			<img class="profile-user-img img-responsive img-circle" src="{{ Gravatar::get($user->email) }}" alt="User profile picture">

			<h3 class="profile-username text-center">Nina Mcintire</h3>

			<p class="text-muted text-center">Software Engineer</p>

			<ul class="list-group list-group-unbordered">
				<li class="list-group-item">
					<b>Amigos</b> <a class="pull-right">1,322</a>
				</li>
				<li class="list-group-item">
					<b>Publicaciones</b> <a class="pull-right">543</a>
				</li>
			</ul>
		</div>
		<!-- /.box-body -->
	</div>

	<div class="box box-default">
		<div class="box-header with-border">
			<h3 class="box-title">Acerca de mi</h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<strong><i class="fa fa-book margin-r-5"></i> Educación</strong>

			<p class="text-muted">
				B.S. in Computer Science from the University of Tennessee at Knoxville
			</p>

			<hr>

			<strong><i class="fa fa-map-marker margin-r-5"></i> Vivo en</strong>

			<p class="text-muted">Malibu, California</p>

			<hr>

			<strong><i class="fa fa-file-text-o margin-r-5"></i> Nota</strong>

			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
		</div>
		<!-- /.box-body -->
	</div>
</div>
<div class="col-sm-12 col-md-7">
	<form id="formPost">
		<div class="input-group">
			<input type="text" class="form-control black-color" placeholder="Publicar...">
			<span class="input-group-btn">
				<button class="btn btn-default" type="button" onclick="openPostFile(this)"><i class="fa fa-camera fa-lg" aria-hidden="true"></i></button>
			</span>
		</div><!-- /input-group -->
	</form>
	<br>
	<ul class="timeline">
		<!-- timeline item -->
		<li>
			<!-- timeline icon -->
			<i class="fa fa-camera bg-blue" aria-hidden="true"></i>
			<div class="timeline-item">
				<div class="box box-widget">
					<div class="box-header with-border">
						<div class="user-block">
							<img class="img-circle" src="{{ Gravatar::get($user->email) }}" alt="User Image">
							<span class="username"><a href="#">Jonathan Burke Jr.</a></span>
							<span class="description">Shared publicly - 7:30 PM Today</span>
						</div>
						<!-- /.user-block -->
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<img class="img-responsive pad image" src="http://lorempixel.com/g/1000/600" alt="Photo">

						<p>I took this photo this morning. What do you guys think?</p>
						<a href="post" type="button" class="btn btn-default btn-xs"><i class="fa fa-eye"></i> Ver</a>
						<button type="button" onclick="sharePost(this)" url="{{ url('/') }}" class="btn btn-default btn-xs"><i class="fa fa-share"></i> Compartir</button>
						<button type="button" class="btn btn-default btn-xs"><i class="fa fa-thumbs-o-up"></i> Sharp</button>
						<span class="pull-right text-muted">127 likes - 3 comments</span>
					</div>
					<!-- /.box-body -->
					<div class="box-footer box-comments">
						<div class="box-comment">
							<!-- User image -->
							<img class="img-circle img-sm" src="{{ Gravatar::get($user->email) }}" alt="User Image">

							<div class="comment-text">
								<span class="username">
									Maria Gonzales
									<span class="text-muted pull-right">8:03 PM Today</span>
								</span><!-- /.username -->
								It is a long established fact that a reader will be distracted
								by the readable content of a page when looking at its layout.
							</div>
							<!-- /.comment-text -->
						</div>
						<!-- /.box-comment -->
						<div class="box-comment">
							<!-- User image -->
							<img class="img-circle img-sm" src="{{ Gravatar::get($user->email) }}" alt="User Image">

							<div class="comment-text">
								<span class="username">
									Luna Stark
									<span class="text-muted pull-right">8:03 PM Today</span>
								</span><!-- /.username -->
								It is a long established fact that a reader will be distracted
								by the readable content of a page when looking at its layout.
							</div>
							<!-- /.comment-text -->
						</div>
						<!-- /.box-comment -->
					</div>
					<!-- /.box-footer -->
					<div class="box-footer">
						<form action="#" method="post">
							<img class="img-responsive img-circle img-sm" src="{{ Gravatar::get($user->email) }}" alt="Alt Text">
							<!-- .img-push is used to add margin to elements next to floating images -->
							<div class="img-push">
								<input type="text" class="form-control input-sm" placeholder="Presione enter para publicar el comentario" style="color: #555;">
							</div>
						</form>
					</div>
					<!-- /.box-footer -->
				</div>
				<div class="timeline-footer text-center">
				</div>
			</div>
		</li>
		<!-- END timeline item -->
		<li>
			<!-- timeline icon -->
			<i class="fa fa-camera bg-blue" aria-hidden="true"></i>
			<div class="timeline-item">
				<div class="box box-widget">
					<div class="box-header with-border">
						<div class="user-block">
							<img class="img-circle" src="{{ Gravatar::get($user->email) }}" alt="User Image">
							<span class="username"><a href="#">Jonathan Burke Jr.</a></span>
							<span class="description">Shared publicly - 7:30 PM Today</span>
						</div>
						<!-- /.user-block -->
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<img class="img-responsive pad image" src="http://lorempixel.com/1200/800" alt="Photo">
						<p>I took this photo this morning. What do you guys think?Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
							cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
							proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
							<a href="post" type="button" class="btn btn-default btn-xs"><i class="fa fa-eye"></i> Ver</a>
							<button type="button" onclick="sharePost(this)" url="{{ url('/') }}" class="btn btn-default btn-xs"><i class="fa fa-share"></i> Share</button>
							<button type="button" class="btn btn-default btn-xs"><i class="fa fa-thumbs-o-up"></i> Sharp</button>
							<span class="pull-right text-muted">127 likes - 3 comments</span>
						</div>
						<!-- /.box-body -->
						<div class="box-footer box-comments">
							<div class="box-comment">
								<!-- User image -->
								<img class="img-circle img-sm" src="{{ Gravatar::get($user->email) }}" alt="User Image">

								<div class="comment-text">
									<span class="username">
										Maria Gonzales
										<span class="text-muted pull-right">8:03 PM Today</span>
									</span><!-- /.username -->
									It is a long established fact that a reader will be distracted
									by the readable content of a page when looking at its layout.
								</div>
								<!-- /.comment-text -->
							</div>
							<!-- /.box-comment -->
							<div class="box-comment">
								<!-- User image -->
								<img class="img-circle img-sm" src="{{ Gravatar::get($user->email) }}" alt="User Image">

								<div class="comment-text">
									<span class="username">
										Luna Stark
										<span class="text-muted pull-right">8:03 PM Today</span>
									</span><!-- /.username -->
									It is a long established fact that a reader will be distracted
									by the readable content of a page when looking at its layout.
								</div>
								<!-- /.comment-text -->
							</div>
							<!-- /.box-comment -->
						</div>
						<!-- /.box-footer -->
						<div class="box-footer">
							<form action="#" method="post">
								<img class="img-responsive img-circle img-sm" src="{{ Gravatar::get($user->email) }}" alt="Alt Text">
								<!-- .img-push is used to add margin to elements next to floating images -->
								<div class="img-push">
									<input type="text" class="form-control input-sm" placeholder="Presione enter para publicar el comentario" style="color: #555;">
								</div>
							</form>
						</div>
						<!-- /.box-footer -->
					</div>
					<div class="timeline-footer text-center">
					</div>
				</div>
			</li>
			<li>
				<i class="fa fa-clock-o bg-gray"></i>
			</li>
		</ul>
		<div class="text-center">
			<nav aria-label="Page navigation">
				<ul class="pagination">
					<li>
						<a href="#" aria-label="Previous">
							<span aria-hidden="true">&laquo;</span>
						</a>
					</li>
					<li><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
					<li>
						<a href="#" aria-label="Next">
							<span aria-hidden="true">&raquo;</span>
						</a>
					</li>
				</ul>
			</nav>
			<!-- /.box-tools -->
		</div>
		<!-- /.box-header -->
		<!-- /.box-footer -->
	</div>
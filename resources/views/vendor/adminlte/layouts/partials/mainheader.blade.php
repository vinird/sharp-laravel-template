<header class="main-header">
    <nav class="navbar navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <a href="{{ url('home') }}" class="navbar-brand"><b>Sharp</b></a>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                    <i class="fa fa-bars"></i>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="/home">Inicio <span class="sr-only">(current)</span></a></li>
                    <li><a href="{{ url('/profile') }}">Perfil</a></li>
                    <li><a href="#" onclick="seeAllNotifications()">Notificaciones</a></li>
                </ul>

                <ul class="nav navbar-nav">
                    <li class="dropdown messages-menu" id="li-navbar-search-input">
                        <a href="#" data-toggle="" style="padding: 10px;" class="trasmparent-background">
                            <input type="text" class="form-control input-sm black-color" id="navbar-search-input" placeholder="Buscar usuario.." >
                        </a>
                        <ul class="dropdown-menu" id="dropdown-navbar-search-input">
                            <li>
                                <!-- inner menu: contains the messages -->
                                <ul class="menu">
                                    <li><!-- ng-repeat -->
                                        <a href="#">
                                            <div class="pull-left">
                                                <!-- User Image -->
                                                <img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image"/>
                                            </div>
                                            <p><h5>Nombre de usuario</h5></p>
                                        </a>
                                    </li><!-- end message -->
                                </ul><!-- /.menu -->
                            </li>
                            <li class="footer"></li>
                        </ul>
                    </li><!-- /.messages-menu -->
                </ul>

            </div>
            <!-- /.navbar-collapse -->
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->
                    <li class="dropdown messages-menu">
                        <!-- Menu toggle button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-envelope-o"></i>
                            <span class="label label-success">4</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">{{ trans('adminlte_lang::message.tabmessages') }}</li>
                            <li>
                                <!-- inner menu: contains the messages -->
                                <ul class="menu">
                                    <li><!-- start message -->
                                        <a href="#" onclick="openMessagesModal()">
                                            <div class="pull-left">
                                                <!-- User Image -->
                                                <img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image"/>
                                            </div>
                                            <!-- Message title and timestamp -->
                                            <h4>
                                                {{ trans('adminlte_lang::message.supteam') }}
                                                <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                            </h4>
                                            <!-- The message -->
                                            <p>{{ trans('adminlte_lang::message.awesometheme') }}</p>
                                        </a>
                                    </li><!-- end message -->
                                </ul><!-- /.menu -->
                            </li>
                            <li class="footer"><a href="#">
                                <button type="button" onclick="openNewMessagesModal()" class="btn btn-block btn-primary btn-xs">
                                    <i class="fa fa-paper-plane" aria-hidden="true"></i> 
                                    Nuevo mensaje</button>
                                </a></li>
                            </ul>
                        </li><!-- /.messages-menu -->

                        <!-- Notifications Menu -->
                        <li class="dropdown notifications-menu">
                            <!-- Menu toggle button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-bell-o"></i>
                                <span class="label label-warning">10</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">{{ trans('adminlte_lang::message.notifications') }}</li>
                                <li>
                                    <!-- Inner Menu: contains the notifications -->
                                    <ul class="menu">
                                        <li><!-- start notification -->
                                            <a href="#" onclick="seeNotification()">
                                                <i class="fa fa-users text-aqua"></i> {{ trans('adminlte_lang::message.newmembers') }}
                                            </a>
                                        </li><!-- end notification -->
                                    </ul>
                                </li>
                                <li class="footer"><a href="#" onclick="seeAllNotifications()">Ver todas</a></li>
                            </ul>
                        </li>
                        <!-- User Account Menu -->
                        @if (Auth::guest())
                        <li><a href="{{ url('/register') }}">{{ trans('adminlte_lang::message.register') }}</a></li>
                        <li><a href="{{ url('/login') }}">{{ trans('adminlte_lang::message.login') }}</a></li>
                        @else
                        <li class="dropdown user user-menu">
                            <!-- Menu Toggle Button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <!-- The user image in the navbar-->
                                <img src="{{ Gravatar::get($user->email) }}" class="user-image" alt="User Image"/>
                                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                <span class="hidden-xs">{{ Auth::user()->name }}</span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- The user image in the menu -->
                                <li class="user-header">
                                  <img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image">

                                  <p>
                                    Alexander Pierce - Web Developer
                                    <small>Member since Nov. 2012</small>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <!-- Menu Body -->
                            <li class="user-body">
                               <div class="row">
                                   <div class="col-xs-12 text-center">
                                       <a href="#" onclick="openModalPassword()">Cambiar contraseña</a>
                                   </div>
                               </div>
                           </li>
                           <!-- Menu Footer-->
                           <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ url('/profile') }}" class="btn btn-default btn-flat">{{ trans('adminlte_lang::message.profile') }}</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{ url('/logout') }}" class="btn btn-default btn-flat"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                {{ trans('adminlte_lang::message.signout') }}
                            </a>

                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                                <input type="submit" value="logout" style="display: none;">
                            </form>

                        </div>
                    </li>
                </ul>
            </li>
            @endif
        </ul>
    </div>
    <!-- /.navbar-custom-menu -->
</div>
<!-- /.container-fluid -->
</nav>
</header>
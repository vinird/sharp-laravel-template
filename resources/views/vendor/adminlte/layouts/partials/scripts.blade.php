<!-- REQUIRED JS SCRIPTS -->

<!-- JQuery and bootstrap are required by Laravel 5.3 in resources/assets/js/bootstrap.js-->
<!-- Laravel App -->
<script src="{{ asset('/js/app.js') }}" type="text/javascript"></script>

<!-- Vieww\er -->
<script src="{{ asset('/js//viewer/viewer.min.js') }}" type="text/javascript"></script>

<!-- Select2 -->
<script src="{{ asset('/js/select2/select2.full.min.js') }}" type="text/javascript"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->
<script src="{{ asset('/js/slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>

<!-- Custom -->
<script src="{{ asset('/js/custom.js') }}" type="text/javascript"></script>

<!-- Toastr -->
<script src="{{ asset('/js/toastr/toastr.min.js') }}" type="text/javascript"></script>

<script>
  @if(Session::has('notification'))
	toastr.options = {
	  "closeButton": false,
	  "debug": false,
	  "newestOnTop": false,
	  "progressBar": true,
	  "positionClass": "toast-top-center",
	  "preventDuplicates": false,
	  "onclick": null,
	  "showDuration": "100",
	  "hideDuration": "1000",
	  "timeOut": "7000",
	  "extendedTimeOut": "1000",
	  "showEasing": "swing",
	  "hideEasing": "linear",
	  "showMethod": "fadeIn",
	  "hideMethod": "fadeOut"
	}
    var type = "{{ Session::get('notification.alert-type', 'info') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('notification.message') }}");
            break;
        
        case 'warning':
            toastr.warning("{{ Session::get('notification.message') }}");
            break;
        case 'success':
            toastr.success("{{ Session::get('notification.message') }}");
            break;
        case 'error':
            toastr.error("{{ Session::get('notification.message') }}");
            break;
    }
  @endif
  // http://www.expertphp.in/article/laravel-5-3-notification-message-popup-using-toastr-js-plugin-with-demo
  {{ session()->forget('notification') }}
</script>
<script>
    window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
    ]) !!};
</script>
</script>
@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<div class="container spark-screen">
	<br>
	<div class="row">
		<!-- Columna izquierda  -->
		@include('vendor.adminlte.layouts.partials-home.left-col')

		<!-- Columna central -->
		@include('vendor.adminlte.layouts.partials-home.main-col')

		<!-- Columna derecha -->
		@include('vendor.adminlte.layouts.partials-home.right-col')

	</div>
</div>
@endsection

@section('custom-scripts')
@endsection
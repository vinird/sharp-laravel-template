<br>
<div class="box box-widget">
	<div class="box-header with-border">
		<div class="user-block">
			<img class="img-circle" src="{{ Gravatar::get($user->email) }}" alt="User Image">
			<span class="username"><a href="#">Jonathan Burke Jr.</a></span>
			<span class="description">Shared publicly - 7:30 PM Today</span>
		</div>
		<!-- /.user-block -->
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<img class="img-responsive pad image" src="http://lorempixel.com/g/1000/600" alt="Photo">

		<p>I took this photo this morning. What do you guys think?</p>
		<button type="button" class="btn btn-default btn-xs"><i class="fa fa-share"></i> Compartir</button>
		<button type="button" class="btn btn-default btn-xs"><i class="fa fa-thumbs-o-up"></i> Sharp</button>
		<span class="pull-right text-muted">127 likes - 3 comments</span>
	</div>
	<!-- /.box-body -->
	<div class="box-footer box-comments">
		<div class="box-comment">
			<!-- User image -->
			<img class="img-circle img-sm" src="{{ Gravatar::get($user->email) }}" alt="User Image">

			<div class="comment-text">
				<span class="username">
					Maria Gonzales
					<span class="text-muted pull-right">8:03 PM Today</span>
				</span><!-- /.username -->
				It is a long established fact that a reader will be distracted
				by the readable content of a page when looking at its layout.
			</div>
			<!-- /.comment-text -->
		</div>
		<!-- /.box-comment -->
		<div class="box-comment">
			<!-- User image -->
			<img class="img-circle img-sm" src="{{ Gravatar::get($user->email) }}" alt="User Image">

			<div class="comment-text">
				<span class="username">
					Luna Stark
					<span class="text-muted pull-right">8:03 PM Today</span>
				</span><!-- /.username -->
				It is a long established fact that a reader will be distracted
				by the readable content of a page when looking at its layout.
			</div>
			<!-- /.comment-text -->
		</div>
		<!-- /.box-comment -->
	</div>
	<!-- /.box-footer -->
	<div class="box-footer">
		<form action="#" method="post">
			<img class="img-responsive img-circle img-sm" src="{{ Gravatar::get($user->email) }}" alt="Alt Text">
			<!-- .img-push is used to add margin to elements next to floating images -->
			<div class="img-push">
				<input type="text" class="form-control input-sm" placeholder="Presione enter para publicar el comentario" style="color: #555;">
			</div>
		</form>
	</div>
	<!-- /.box-footer -->
</div>
@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Publication
@endsection


@section('main-content')
<div class="container spark-screen">
	@include('posts.partials.main-content')
</div>
@endsection

@section('custom-scripts')

@endsection
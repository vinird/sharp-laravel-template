### Proceso de intalación

- Descargar el framework con la plantilla adminLTE: ``git clone https://vinird@bitbucket.org/vinird/sharp-laravel-template.git``

- Ingresar al directorio del proyecto: ``cd sharp-laravel-template``

- Intarlar las dependencias: ``composer update``

- Crear el archivo ``.env`` en la raíz del proyecto

- Copiar y pegar el contenido de ``.env.example`` a ``.env``

- Crear una base de datos para el proyecto

- Modificar el arhivo ``.env`` con las variables de conexión a la base de datos


- Migrar la base de datos: ``php artisan migrate``

- Sembrar la base de datos: ``php artisan db:seed``

- Generar llave: ``php artisan key:generate``

- Levantar servidor: ``php artisan serve``

- Ingresar al sistema con las siguientes credenciales: correo ``admin@admin.com`` contraseña ``password`` 
<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        // Usuarios
        DB::table('users')->insert([
            'name'      => "Administrador",
            'email'     => "admin@admin.com",
            'password'  => bcrypt('password')]);
        DB::table('users')->insert([
            'name'      => "María",
            'email'     => "admin@admin2.com",
            'password'  => bcrypt('password')]);
        DB::table('users')->insert([
            'name'      => "Alejandor",
            'email'     => "admin@admin3.com",
            'password'  => bcrypt('password')]);
        DB::table('users')->insert([
            'name'      => "Carlos",
            'email'     => "admin@admin4.com",
            'password'  => bcrypt('password')]);
        DB::table('users')->insert([
            'name'      => "Juan",
            'email'     => "admin@admin5.com",
            'password'  => bcrypt('password')]);
    }
}

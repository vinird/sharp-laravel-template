jQuery(document).ready(function($) {
	// Galería //////////////////////////////////////////////////////
	$('.flex-images').flexImages({rowHeight: 300, truncate: false});
});

// Abrir modal modificar perfil ///////////////////////////////////
function abrirModalModificarPerfil(){
	$('#modalModificarPerfil').modal({
		show: true,
		backdrop: false
	});
}

// Abrir modal modificar foto de portada //////////////////////////
function abrirModalModificarFotoPortada(){
	$('#modal-change-main-photo').modal({
		show: true,
		backdrop: false
	});
}

// Abrir modal modificar imagen de perfil //////////////////////////
function abrirModalModificarImagenPerfil(){
	$('#modal-change-profile-image').modal({
		show: true,
		backdrop: false
	});
}

// Abrir modal eliminar publicación ///////////////////////////
function abrirModalEliminarPublicacion(){
	$('#modal-delete-posts').modal({
		show: true,
		backdrop: false
	});
}

// Eliminar post ///////////////////////////////////////////////
function btnConfirmarEliminarPost(button){
	$(button).attr('class', 'btn btn-danger btn-sm');
	$(button).attr('onclick', 'btnEliminarPost(this)');
}

function btnEliminarPost(button){
	console.log(button);
}
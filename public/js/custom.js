jQuery(document).ready(function($) {

	/////////////////////////////////////////////////////////////
	// viewer
	$('.image').viewer({
		toolbar: false,
		movable: false,
		navbar: false,
	});
	$( '.images' ).viewer({
		toolbar: false,
		movable: false, 
		navbar: false,
	});

	/////////////////////////////////////////////////////////////
	// Slect2
	$('.select2').select2();
	$( "#dropdown" ).select2({
		theme: "bootstrap"
	});

	/////////////////////////////////////////////////////////////
    // Buscar usuario
    countKeys = 0;
    $('#navbar-search-input').keydown(function(event) {
    	if(countKeys == 0){
    		$('#dropdown-navbar-search-input').dropdown('toggle')
    		$('#dropdown-navbar-search-input').removeClass('hide')
    	}
    	countKeys++;

    	// Lógica aquí
    });
    $('#navbar-search-input').focusout(function(event) {
    	$('#dropdown-navbar-search-input').addClass('hide');
    	$('#li-navbar-search-input').removeClass('open');
    	countKeys = 0;

    	// Lógica aquí
    });
	
});

////////////////////////////////////////////
// Mensajes
function openMessagesModal() {
	$('#modal-messages').modal({
		show: true,
		backdrop: false
	});
}
function openNewMessagesModal(){
	$('#modal-new-messages').modal({
		show: true,
		backdrop: false
	});
}
/////////////////////////////////////////////
// Notificaciones
function seeNotification(){
	$('#modal-see-notification').modal({
		show: true,
		backdrop: false
	});
}
function seeAllNotifications(){
	$('#modal-all-notifications').modal({
		show: true,
		backdrop: false
	});
}

///////////////////////////////////////////
// Cambiar contraseñe
function openModalPassword(){
	$('#modal-change-password').modal({
		show: true,
		backdrop: false
	});
}

//////////////////////////////////////////
// Abrir ventana de archivo para publicación
function openPostFile(){
    $('#formPost input[type="file"]').remove();
	var input = $(document.createElement('input')); 
    input.addClass('hide');
    input.attr("type", "file");
    $('#formPost').append(input);
    input.trigger('click');
    return false;
}
$('#formPost').submit(function(){
	return false;
})

//////////////////////////////////////////
// Compartir publicación 
function sharePost(button){
	url = $(button).attr('url');
	$('#shareInputValue').val(url);
	$('#modal-share-post').modal({
		show: true,
		backdrop: false
	});
}
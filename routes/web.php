<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});
//////////// Ver perfil //////////////
Route::get('/profile', function(){
	$notification = array(
		'message' => 'Esto es un ejemplo del plugin Toastr. Para implementarlo se utilizan variables de sesión de PHP. Puede ver el código en "routes/web.php" y "views/vendor/adminlte/layouts/partials/scripts.blade.php" ', 
		'alert-type' => 'success'
		);
	session()->set('notification',$notification);
	return view('profile.main');
})->middleware('auth');

///////////// Ver publicación ////////
Route::get('/post', function(){
	return view('posts.main');
});
